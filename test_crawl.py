import os
import os.path
import re

from selenium import webdriver

import crawl

TESTCRAWL_FILE = 'testcrawl'

STORY_URL_REGEXP = 'https://mcstories\.com/.*/index\.html'
STORIES_BY_LETTER_URL_REGEXP = 'https://mcstories\.com/Titles/.\.html'
AUTHOR_URL_REGEXP = 'https://mcstories\.com/Authors/.*\.html'
TAG_URL_REGEXP = 'https://mcstories\.com/Tags/.*\.html'
READERS_PICK_URL_REGEXP = 'https://mcstories\.com/ReadersPicks/.*\.html'

WILD = '.*'

def test_get_links():
    test_cases = [
        # what's new
        ('http://mcstories.com/WhatsNew.html', STORY_URL_REGEXP),

        # authors index
        ('http://mcstories.com/Authors/index.html', AUTHOR_URL_REGEXP),

        # single author's page
        ('http://mcstories.com/Authors/Zapped!.html', STORY_URL_REGEXP),

        # tags index
        ('http://mcstories.com/Tags/index.html', TAG_URL_REGEXP),

        # single tag page
        ('http://mcstories.com/Tags/ca.html', STORY_URL_REGEXP),

        # readers pick index
        ('http://mcstories.com/ReadersPicks/index.html', READERS_PICK_URL_REGEXP),

        # # single reader's pick page
        ('http://mcstories.com/ReadersPicks/RBA.html', STORY_URL_REGEXP),

        # titles index
        ('http://mcstories.com/Titles/index.html', STORIES_BY_LETTER_URL_REGEXP),

        # titles by letter
        ('http://mcstories.com/Titles/X.html', STORY_URL_REGEXP),

        # story landing page, single chapter (TODO)
        # ('http://mcstories.com/CandaceFalls/index.html', WILD),

        # story landing page, multi-chapter (TODO)
        # ('http://mcstories.com/AdventuresOfEggyBookTwo/index.html', WILD)
    ]

    driver = webdriver.Chrome()

    for case in test_cases:
        driver.get(case[0])
        links = crawl.get_links(driver)

        assert links
        pattern = re.compile(case[1])
        for l in links[:10]:
            if l:
                assert pattern.match(l)

def test_get_links_no_results():
    urls = [
        # story chapter (single-chap story)
        'http://mcstories.com/CandaceFalls/CandaceFalls.html',
        # story chapter (multi-chap story)
        'http://mcstories.com/AdventuresOfEggyBookTwo/AdventuresOfEggyBookTwo1.html'
    ]

    driver = webdriver.Chrome()

    for url in urls:
        driver.get(url)
        links = crawl.get_links(driver)
        assert not links

def test_is_story_body():
    test_cases = [
        # story chapter (single-chap story)
        ('http://mcstories.com/CandaceFalls/CandaceFalls.html', True),

        # story chapter (multi-chap story)
        ('http://mcstories.com/AdventuresOfEggyBookTwo/AdventuresOfEggyBookTwo1.html', True),

        # what's new
        ('http://mcstories.com/WhatsNew.html', False),

        # authors index
        ('http://mcstories.com/Authors/index.html', False),

        # single author's page
        ('http://mcstories.com/Authors/Zapped!.html', False),

        # tags index
        ('http://mcstories.com/Tags/index.html', False),

        # single tag page
        ('http://mcstories.com/Tags/ca.html', False),

        # readers pick index
        ('http://mcstories.com/ReadersPicks/index.html', False),

        # single reader's pick page
        ('http://mcstories.com/ReadersPicks/RBA.html', False),

        # titles index
        ('http://mcstories.com/Titles/index.html', False),

        # titles by letter
        ('http://mcstories.com/Titles/X.html', False),

        # story landing page, single chapter
        ('http://mcstories.com/CandaceFalls/index.html', False),

        # story landing page, multi-chapter
        ('http://mcstories.com/AdventuresOfEggyBookTwo/index.html', False)
    ]

    driver = webdriver.Chrome()

    for case in test_cases:
        driver.get(case[0])
        assert crawl.is_story_body(driver) == case[1]


def test_is_story_landing_page():
    test_cases = [
        # story chapter (single-chap story)
        ('http://mcstories.com/CandaceFalls/CandaceFalls.html', False),

        # story chapter (multi-chap story)
        ('http://mcstories.com/AdventuresOfEggyBookTwo/AdventuresOfEggyBookTwo1.html', False),

        # what's new
        ('http://mcstories.com/WhatsNew.html', False),

        # authors index
        ('http://mcstories.com/Authors/index.html', False),

        # single author's page
        ('http://mcstories.com/Authors/Zapped!.html', False),

        # tags index
        ('http://mcstories.com/Tags/index.html', False),

        # single tag page
        ('http://mcstories.com/Tags/ca.html', False),

        # readers pick index
        ('http://mcstories.com/ReadersPicks/index.html', False),

        # single reader's pick page
        ('http://mcstories.com/ReadersPicks/RBA.html', False),

        # titles index
        ('http://mcstories.com/Titles/index.html', False),

        # titles by letter
        ('http://mcstories.com/Titles/X.html', False),

        # story landing page, single chapter
        ('http://mcstories.com/CandaceFalls/index.html', True),

        # story landing page, multi-chapter
        ('http://mcstories.com/AdventuresOfEggyBookTwo/index.html', True)
    ]

    driver = webdriver.Chrome()

    for case in test_cases:
        driver.get(case[0])
        assert crawl.is_story_landing_page(driver) == case[1]


def test_scrape_story_body():
    assert not os.path.exists(TESTCRAWL_FILE)

    driver = webdriver.Chrome()
    driver.get('http://mcstories.com/BacheloretteParty/BacheloretteParty.html')

    try:
        with open(TESTCRAWL_FILE, 'a') as outfile:
            crawl.scrape_story_body(driver, outfile)

        assert os.path.exists(TESTCRAWL_FILE)
        assert os.path.isfile(TESTCRAWL_FILE)

        with open(TESTCRAWL_FILE) as infile:
            body = infile.read()

        assert len(body) > 0

    finally:
        try:
            os.remove(TESTCRAWL_FILE)
        except OSError:
            pass


def test_scrape_blurb():
    assert not os.path.exists(TESTCRAWL_FILE)

    driver = webdriver.Chrome()
    driver.get('http://mcstories.com/AbbysAscension/index.html')

    try:
        with open(TESTCRAWL_FILE, 'a') as outfile:
            crawl.scrape_blurb(driver, outfile)

        assert os.path.exists(TESTCRAWL_FILE)
        assert os.path.isfile(TESTCRAWL_FILE)

        with open(TESTCRAWL_FILE) as infile:
            body = infile.read()

        assert len(body) > 0

    finally:
        try:
            os.remove(TESTCRAWL_FILE)
        except OSError:
            pass
