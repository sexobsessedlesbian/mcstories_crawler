#! /usr/bin/env python

import argparse
import os.path
import queue

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

MODE_STORY = 'story'
MODE_BLURB = 'blurb'

def _find_elem_if_exists(parent, selector):
    try:
        return parent.find_element_by_css_selector(selector)
    except NoSuchElementException:
        return None


def _get_links_from_table(parent):
    return parent.find_elements_by_css_selector('#mcstories table#index td a')


def _bulk_add_to_queue(q, items):
    for item in items:
        q.put(item)


def _scrape_by_selector(driver, outfile, selector):
    """Scrape the contents of the given selector, dump to outfile."""
    content_elems = driver.find_elements_by_css_selector(selector)
    for elem in content_elems:
        outfile.write(elem.text)
        outfile.write('\n\n')


def argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'start_url',
        type=str,
        help='url to start the crawl at'
    )

    parser.add_argument(
        'mode',
        type=str,
        default=MODE_STORY,
        choices=[MODE_STORY, MODE_BLURB],
        help='crawler mode: scrape full stories ("story") or just summaries ("blurb")'
    )

    parser.add_argument(
        '--outfile',
        type=str,
        default='scrape_result.txt',
        help='name of the file to write output to'
    )

    parser.add_argument(
        '--overwrite',
        action='store_true',
        help='if output file already exists, overwrite it (instead of appending to end)'
    )

    return parser


def is_story_body(driver):
    return bool(_find_elem_if_exists(driver, 'nav.story'))


def is_story_landing_page(driver):
    return (bool(_find_elem_if_exists(driver, '.synopsis')) and
        driver.current_url.endswith('/index.html'))


def scrape_story_body(driver, outfile):
    # Assume we're on a page containing story body -- scrape it!
    _scrape_by_selector(driver, outfile, '#mcstories p:not(.synopsis)')


def scrape_blurb(driver, outfile):
    # Assume we're on a story landing page -- scrape story blurb (i.e. summary).
    _scrape_by_selector(driver, outfile, '#mcstories section.synopsis')


def _look_for_link_elems(driver):
    """
    On the given page, look for DOM elements corresponding to story links (<a> tags).
    """

    # Look for links in a table
    link_elems = driver.find_elements_by_css_selector('#mcstories table#index td a')
    if link_elems:
        return link_elems

    # Otherwise look for links in '.story' div. (Will only pull story links,
    # not author links or others -- we assume situations where we might want
    # to pull other links (e.g. "all FD stories") are table-based views and
    # therefore handled above.
    story_elems = driver.find_elements_by_css_selector('#mcstories .story')
    # HACK: use find_elem instead of find_elemS b/c we only want the first div
    link_elems = [_find_elem_if_exists(elem, 'div a') for elem in story_elems]
    if link_elems:
        return link_elems

    # Otherwise look for links in '.chapter' div
    link_elems = driver.find_elements_by_css_selector('#mcstories div.chapter a')
    if link_elems:
        return link_elems

    # Otherwise look for links in '.pick' sections
    pick_elems = driver.find_elements_by_css_selector('#mcstories .pick')
    # HACK: use find_elem instead of find_elemS b/c we only want the first link
    link_elems = [_find_elem_if_exists(elem, 'a') for elem in pick_elems]
    if link_elems:
        return link_elems

    return []


def get_links(driver):
    """Return a list of links (drawn from the current page) that we want to crawl."""
    link_elems = _look_for_link_elems(driver)
    links = [link.get_attribute('href') for link in link_elems if link]

    # Special case: if we're crawling the titles index, don't include the "all
    # stories" link at the bottom
    if driver.current_url == 'https://mcstories.com/Titles/index.html':
        links.remove('https://mcstories.com/Tags/mc.html')

    return links


def crawl(url, q, driver, outfile, mode=MODE_STORY):
    if 'mcstories.com' not in url:
        print('this script only supports scraping mcstories.com, rejecting url "%s"' % url)
        return

    driver.get(url)
    print('Crawling %s' % url)

    if mode == MODE_STORY:
        if is_story_body(driver):
            print('\tScraping story body from %s' % url)
            scrape_story_body(driver, outfile)
            return
    elif mode == MODE_BLURB:
        if is_story_landing_page(driver):
            scrape_blurb(driver, outfile)
            return
    else:
        raise ValueError('Illegal mode: %s' % mode)

    # Look for links on the page.
    new_links = get_links(driver)
    if new_links:
        # If we found links to crawl in the page, then add them to the queue.
        _bulk_add_to_queue(q, new_links)
        return

    # ...if we got to this point, something has gone wrong.
    print('Whelp, could not process URL: %s' % url)


def crawl_all(q, driver, outfile, mode=MODE_STORY):
    visited = set()
    while not q.empty():
        next_url = q.get()
        if not next_url in visited:
            visited.add(next_url)
            crawl(next_url, q, driver, outfile, mode)


def main():
    parser = argument_parser()
    args = parser.parse_args()

    print('Initializing script on start url %s' % args.start_url)

    if os.path.isfile(args.outfile):
        if args.overwrite:
            print(('Outfile "%s" already exists and "--overwrite" specified; '
                'will overwrite existing file.' % args.outfile))
            os.remove(args.outfile)
        else:
            print('Outfile "%s" already exists; will append to the end.' % args.outfile)

    driver = webdriver.Chrome()
    q = queue.Queue()
    q.put(args.start_url)

    with open(args.outfile, 'a') as outfile:
        crawl_all(q, driver, outfile, args.mode)


if __name__ == '__main__':
    main()
