#! /usr/bin/env python

from selenium import webdriver

if __name__ == '__main__':
    driver = webdriver.Chrome()

    try:
        driver.get('http://localhost:8000/test.html')
        elems = driver.find_elements_by_css_selector('p:not(.also-ignore)')
        for e in elems:
            print(e.text)
    finally:
        driver.close()
