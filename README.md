## Requirements:
* [Chrome driver for Selenium](https://chromedriver.chromium.org/getting-started)
* `python 3.x`
* Python packages: `pip install -r requirements.txt`

## To play around:
In one terminal window, serve the test page: `python -m http.server`.

In another, run the test script: `test.py`.

(It's broken right now, but you can at least try stuff!)
